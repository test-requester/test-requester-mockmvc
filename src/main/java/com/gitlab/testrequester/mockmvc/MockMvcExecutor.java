package com.gitlab.testrequester.mockmvc;

import com.gitlab.testrequester.RequestData;
import com.gitlab.testrequester.RequestExecutor;
import com.gitlab.testrequester.ResponseData;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;

import java.util.Locale;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class MockMvcExecutor implements RequestExecutor {

    private final MockMvc mockMvc;

    @Override
    @NonNull
    @SneakyThrows(Exception.class)
    public ResponseData execute(@NonNull RequestData request) {
        MockHttpServletRequestBuilder mvcRequest = MockMvcRequestBuilders
                .request(request.getMethod(), request.getUri())
                .queryParams(new LinkedMultiValueMap<>(request.getQueryParams()))
                .params(new LinkedMultiValueMap<>(request.getRequestParams()))
                .headers(new HttpHeaders(new LinkedMultiValueMap<>(request.getHeaders())))
                .content(request.getBody());

        MvcResult result = mockMvc.perform(mvcRequest).andReturn();
        MockHttpServletResponse response = result.getResponse();

        return new ResponseData(
                result,
                response.getStatus(),
                response.getHeaderNames().stream().collect(Collectors.toMap(
                        headerName -> headerName.toLowerCase(Locale.ROOT),
                        response::getHeaders
                )),
                response.getContentAsByteArray()
        );
    }
}
