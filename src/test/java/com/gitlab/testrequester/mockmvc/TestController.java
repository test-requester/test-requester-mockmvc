package com.gitlab.testrequester.mockmvc;

import com.gitlab.testrequester.EchoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static java.util.Collections.list;
import static java.util.stream.Collectors.toMap;

@RestController
@RequiredArgsConstructor
public class TestController {

    private final EchoService echoService;

    @RequestMapping(path = "/**")
    public String echo(HttpServletRequest request, @RequestBody(required = false) String body) {
        return echoService.callMe(
                request.getMethod(),
                request.getRequestURI(),
                body,
                list(request.getHeaderNames()).stream().collect(toMap(
                        headerName -> headerName,
                        headerName -> list(request.getHeaders(headerName))
                )),
                list(request.getParameterNames()).stream().collect(toMap(
                        parameterName -> parameterName,
                        parameterName -> listOf(request.getParameterValues(parameterName))
                ))
        );
    }

    @SafeVarargs
    private final <E> List<E> listOf(E... elements) {
        return Collections.unmodifiableList(new ArrayList<>(Arrays.asList(elements)));
    }
}
