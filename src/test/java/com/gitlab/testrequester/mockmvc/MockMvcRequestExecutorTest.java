package com.gitlab.testrequester.mockmvc;

import com.gitlab.testrequester.EchoService;
import com.gitlab.testrequester.RequestExecutor;
import com.gitlab.testrequester.RequestExecutorAbstractTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

class MockMvcRequestExecutorTest extends RequestExecutorAbstractTest {

    @Override
    protected RequestExecutor setupExecutor(EchoService echoService) {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(new TestController(echoService)).build();
        return new MockMvcExecutor(mockMvc);
    }
}
